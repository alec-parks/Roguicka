﻿using System;
using SadConsole;
using Color = Microsoft.Xna.Framework.ColorAnsi;
using Roguicka.Maps;
using RogueSharp.MapCreation;
using RogueSharp;
using Path = System.IO.Path;

namespace Roguicka
{
    public class Program
    {
        private static int _currentConsoleIndex;
        private static RoguickaMap _map;

        private static int _width = 120;
        private static int _height = 40;

        public static void Main(string[] args)
        {
            // Initialize the SadConsole Engine with Size and font
            Engine.Initialize("Assets" + Path.DirectorySeparatorChar + "IBM.font", _width, _height);

            // Hook the start event
            Engine.EngineStart += Engine_EngineStart;

            //Hook the update event to Capture key presses and respond
            Engine.EngineUpdated += Engine_EngineUpdated;

            //PUNCH IT CHEWIE!
            Engine.Run();
        }

        private static void Engine_EngineUpdated(object sender, EventArgs e)
        {
        }

        private static void Engine_EngineStart(object sender, EventArgs e)
        {
            _map = new RoguickaMap(Map.Create(new CaveMapCreationStrategy<Map>(_width, _height, 45, 4, 3)));

            Engine.ConsoleRenderStack.Clear();

            var c = new MapConsole(_map, _width, _height, 30, 80);

            Engine.ActiveConsole = c;
            

            Engine.ConsoleRenderStack.Add(c);
           

            CellAppearance[,] mapData = new CellAppearance[_width,_height];

            for(var x = 0; x < _width; x++) {
                for(var y = 0; y < _height; y++) {
                    RogueSharp.Cell cell = _map.GetCell(x, y);
                    if (cell.IsWalkable) {
                        mapData[cell.X, cell.Y] = new MapObjects.Floor();
                        // Copy the appearance we've defined for Floor or Wall or whatever, to the actual console data that is rendered
                        mapData[cell.X, cell.Y].CopyAppearanceTo(c[cell.X, cell.Y]);
                    }
                    else {
                        mapData[cell.X, cell.Y] = new MapObjects.Wall();
                        mapData[cell.X, cell.Y].CopyAppearanceTo(c[cell.X, cell.Y]);
                    }
                }
            }
        }
    }
}
