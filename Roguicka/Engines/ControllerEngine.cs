﻿using Microsoft.Xna.Framework.Input;
using Roguicka.Components;
using Roguicka.Maps;
using SadConsole.Input;
using System;
using System.Collections.Generic;

namespace Roguicka.Engines {
    public class ControllerEngine {

        public ControllerEngine() {
            movements = new Dictionary<Keys, Tuple<int, int>>();
            movements.Add(Keys.Left, new Tuple<int, int>(-1, 0));
            movements.Add(Keys.Right, new Tuple<int, int>(1, 0));
            movements.Add(Keys.Up, new Tuple<int, int>(0, -1));
            movements.Add(Keys.Down, new Tuple<int, int>(0, 1));
        }

        Dictionary<Keys,Tuple<int,int>> movements;

        public bool Update(RoguickaMap map, KeyboardInfo info, Entity e) {

            if (e.Has<HeroController>()) {

                foreach (KeyValuePair<Keys, Tuple<int, int>> key in movements) {
                    if (info.KeysPressed.Contains(AsciiKey.Get(key.Key)) && map.IsWalkable(e.X + key.Value.Item1, e.Y + key.Value.Item2)) {
                        e.X += key.Value.Item1;
                        e.Y += key.Value.Item2;
                        return true;
                    }
                }
            }
            return false;

        }

    }
}
