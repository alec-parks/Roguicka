﻿using Microsoft.Xna.Framework;
using SadConsole;
using SadConsole.Game;

namespace Roguicka.Components {
    public class RenderComponent: IComponent {

       public GameObject gameObject;

       public int Symbol { get; }
       public Color ForeColor { get; }
       public Color BackColor { get; }
       private bool Visible { get; }

        public RenderComponent(Color fore, Color back, int sym, bool visibility = true) {
            ForeColor = fore;
            BackColor = back;
            Symbol = sym;
            gameObject = new GameObject(Engine.DefaultFont);
            Visible = visibility;
            var animation = new SadConsole.Consoles.AnimatedTextSurface("default",1,1,Engine.DefaultFont);
            var frame = animation.CreateFrame();
            frame[0].GlyphIndex = sym;
            frame[0].Foreground = fore;
            frame[0].Background = back;
            gameObject.Animation = animation;
            gameObject.Position = new Point(0, 0);
            gameObject.Update();
        }

        public void Draw(int X, int Y)
        {
            if (!Visible) return;
            gameObject.Position = new Point(X, Y);
            gameObject.Render();
        }

        public void Update() {
            gameObject.Update();
        }

    }
}
