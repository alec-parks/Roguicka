﻿using System.Collections.Generic;
using System.Linq;

namespace Roguicka.Components {
    public class Entity {

        public int X { get; set; }
        public int Y { get; set; }

        private List<IComponent> Components;

        public Entity(int x, int y) {
            Components = new List<IComponent>();
            X = x;
            Y = y;
        }

        public Entity Add(IComponent component) {
            Components.Add(component);
            return this;
        }

        public bool Has<T>() where T : IComponent {
            return GetComponent<T>() != null;
        }


        public T GetComponent<T>() where T : IComponent {
                return (T)Components.FirstOrDefault(x => x.GetType() == typeof(T));
        }

    }
}
