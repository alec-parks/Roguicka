﻿using SadConsole;
using Color = Microsoft.Xna.Framework.Color;

namespace Roguicka.MapObjects {
    public class Wall : CellAppearance {
        public Wall() : base(Color.White, Color.Gray, 176) {

        }
    }
}
