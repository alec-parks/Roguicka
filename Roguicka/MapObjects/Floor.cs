﻿using SadConsole;
using Color = Microsoft.Xna.Framework.Color;

namespace Roguicka.MapObjects {
    public class Floor: CellAppearance {

        public Floor() : base(Color.DarkGray, Color.Transparent, 46) {

        }

    }
}
