﻿using Microsoft.Xna.Framework;
using Roguicka.Components;
using Roguicka.Engines;
using Roguicka.Maps;
using SadConsole;
using SadConsole.Input;
using System.Collections.Generic;

namespace Roguicka {
    public class MapConsole : SadConsole.Consoles.Console {

        private RoguickaMap _map;

        ControllerEngine _controlEngine;

        List<Entity> _objects;

        public MapConsole(RoguickaMap map, int width, int height, int viewH, int viewW) : base(width,height) {
            _map = map;
            TextSurface.RenderArea = new Rectangle(0, 0, viewW, viewH);

            Engine.Keyboard.RepeatDelay = 0.07f;
            Engine.Keyboard.InitialRepeatDelay = 0.1f;

            _objects = new List<Entity>();
            _controlEngine = new ControllerEngine();

            var x = _map.GetRandomCell();
            var y = _map.GetRandomCell();

            Entity hero = new Entity(x.X,x.Y)
               .Add(new RenderComponent(Color.Aqua, Color.Black, 1))
               .Add(new HeroController());

            TextSurface.RenderArea = new Rectangle(hero.X - (TextSurface.RenderArea.Width / 2),
                                                hero.Y - (TextSurface.RenderArea.Height / 2),
                                                TextSurface.RenderArea.Width, TextSurface.RenderArea.Height);

            Entity hero2 = new Entity(x.X,x.Y+1)
               .Add(new RenderComponent(Color.Red, Color.Black, 128));
               

            _objects.Add(hero);
            _objects.Add(hero2);

        }

        public override bool ProcessKeyboard(KeyboardInfo info) {

            foreach(var entity in _objects) {
                if(_controlEngine.Update(_map, info, entity)) {
                    TextSurface.RenderArea = new Rectangle(entity.X - (TextSurface.RenderArea.Width / 2),
                                                entity.Y - (TextSurface.RenderArea.Height / 2),
                                                TextSurface.RenderArea.Width, TextSurface.RenderArea.Height);
                    
                }
            }

            return false;
        }

        public override void Render() {

            foreach (var entity in _objects)
            {
                if (!entity.Has<RenderComponent>()) continue;
                if (textSurface.RenderArea.Contains(entity.X, entity.Y))
                {
                    var entityRenderComponent = entity.GetComponent<RenderComponent>();
                    entityRenderComponent.gameObject.RenderOffset = this.Position - TextSurface.RenderArea.Location;
                    entityRenderComponent.Draw(entity.X, entity.Y);
                }
            }


            base.Render();
        }
        public override void Update() {
            foreach (var entity in _objects) {
                if (entity.Has<RenderComponent>()) {
                    entity.GetComponent<RenderComponent>().Update();
                }

            }
            base.Update();
        }

    }
}
